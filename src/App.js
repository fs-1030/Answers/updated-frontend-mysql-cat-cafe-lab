import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AllCats from "../src/CatPages/AllCats";
import CatProfile from "../src/CatPages/CatProfile";
import AddCatForm from '../src/CatPages/AddCatForm.js';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route exact path="/" element={<AllCats />} />
          <Route exact path="/cats/:id" element={<CatProfile />} />
          <Route exact path="/add-cat" add-cat element={<AddCatForm />} />
          <Route
            exact
            path="/cats/:id"
            element={<CatProfile />}
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;


