import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

const AllCats = (props) => {
  console.log(props);
  const [cats, setCats] = useState([]);

  const navigate = useNavigate();

  const addCatRoute = (event, cat) => {
    event.preventDefault();
    // console.log(id);
    let path = `/add-cat`;
    navigate(path);
  };

  const catProfileRoute = (event, cat) => {
    event.preventDefault();
    // console.log(id);
    let path = `/cats/${cat.id}`;
    navigate(path);
  };

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:3001/api/cats");
      res
        .json()
        .then((res) => setCats(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, []);

  console.log(cats);

  return (
    <div>
      <h1>Jen's Cat Adoption Cafe Website</h1>
      <button onClick={(e) => addCatRoute(e)}>Add a Cat</button>
      {cats.map((cat) => (
        <div key={cat.id}>
          <p>{cat.name}</p>
          <img src={cat.image} alt={cat.name} width="300" height="300" />
          <button onClick={(e) => catProfileRoute(e, cat)}>View Cat Profile</button>
        </div>
      ))}
    </div>
  );
};

export default AllCats;


