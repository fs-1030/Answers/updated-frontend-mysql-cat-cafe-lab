import React, { useState } from "react";
import { useNavigate } from "react-router-dom"



const AddCatForm = () => {
  const [newCat, setNewCat] = useState({ name: "", image: "" });
  const navigate = useNavigate();

  const handleChange = (event) => {
    setNewCat((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = (event) => {
    fetch("/api/cats", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },

      //make sure to serialize your JSON body
      body: JSON.stringify(newCat),
    }).then((response) => response.json());
    navigate("/");
  };

  return (
    <div>
      <h1>This is the add cat form</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>
            Name:
            <input
              type="text"
              name="name"
              value={newCat.name}
              onChange={handleChange}
            />
          </label>
        </div>
        <div>
          <label>
            Photo:
            <input
              type="text"
              name="image"
              value={newCat.image}
              onChange={handleChange}
            />
          </label>
        </div>
        <input type="submit" value="Submit" />
      </form>
    </div>
  );
};

export default AddCatForm;
